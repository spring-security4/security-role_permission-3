package com.example.springsecurityrolepermission.controller;

import com.example.springsecurityrolepermission.model.Employee;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("manager/api/v1/employee")
public class EmployeeController {

    @GetMapping("/{id}")
    public Employee getById(@PathVariable Long id) {
        return EMPLOYEES.stream()
                .filter(employee -> id.equals(employee.getId()))
                .findFirst()
                .orElseThrow(() -> new IllegalStateException("Employee " + id + " not found"));
    }

    @GetMapping
    public List<Employee> getAllEmployee() {
        return EMPLOYEES;
    }

    @PostMapping
    public String createEmployee(@RequestBody Employee employee) {
        employee.setEmployeeCode(UUID.randomUUID().toString());
        EMPLOYEES.add(employee);
        return "Employee Created Succesfully";
    }

    @PutMapping("/{id}")
    public String updateEmployee(@PathVariable Long id,@RequestBody Employee employee){
        return "Updated succesfully";
    }



    @DeleteMapping("/{id}")
    public String deleteEmployee(@PathVariable Long id){
        return "Employee deleted succesfully";
    }


    private final List<Employee> EMPLOYEES = new ArrayList<>(Arrays.asList(
            new Employee(1L, "Yasin", "Quliyev", UUID.randomUUID().toString()),
            new Employee(2L, "Ismayil", "Camalov", UUID.randomUUID().toString()),
            new Employee(3L, "Fuad", "Ahmadov", UUID.randomUUID().toString())
    ));
}
