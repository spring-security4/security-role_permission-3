package com.example.springsecurityrolepermission;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringSecurityRolePermission3 {

    public static void main(String[] args) {
        SpringApplication.run(SpringSecurityRolePermission3.class, args);
    }

}
