package com.example.springsecurityrolepermission.securityconfig;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

import static com.example.springsecurityrolepermission.securityconfig.UserPermission.*;
import static com.example.springsecurityrolepermission.securityconfig.UserRole.*;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class ApplicationSecurityConfig extends WebSecurityConfigurerAdapter {


    private final PasswordEncoder passwordEncoder;

    /**
     * Burdaki acccess de biz customer e verdiyimiz access yalniz get by id dir
     * Manager e ise hem customer ehmdeki employee deki endpointlerde get metodlarina access vermisik
     * Admin e ise butun accessleri vermisik
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .cors().disable()
                .csrf().disable()
                .authorizeRequests()
                .antMatchers("/", "index", "/css/*", "/js/*").permitAll() // burda her hansisa bir login teleb etmiyecek
                .antMatchers(HttpMethod.GET, "/api/**/by-id/*").hasAuthority(CUSTOMER_READ.getPermission())
                .antMatchers(HttpMethod.GET, "/api/**/").hasAnyRole(MANAGER.name(), ADMIN.name())
                .antMatchers("/api/**").hasRole(ADMIN.name())
                .antMatchers(HttpMethod.GET, "/manager/api/**").hasAnyRole(MANAGER.name(), ADMIN.name())
                .antMatchers("/manager/api/**").hasRole(ADMIN.name())
                .anyRequest().authenticated()
                .and()
                .httpBasic();
    }

    @Override
    @Bean
    protected UserDetailsService userDetailsService() {
        UserDetails elchin = User.builder()  // burda yazdigimiz username password ile endpointleri cagiracayiq
                .username("el349")
                .password(passwordEncoder.encode("1234"))
                .authorities(ADMIN.getGrantedAuthorities())
                .build();

        UserDetails ilqar = User.builder()  // burda yazdigimiz username password ile endpointleri cagiracayiq
                .username("ilqar123")
                .password(passwordEncoder.encode("1234"))
                .authorities(MANAGER.getGrantedAuthorities())
                .build();

        UserDetails lale = User.builder()  // burda yazdigimiz username password ile endpointleri cagiracayiq
                .username("lale123")
                .password(passwordEncoder.encode("1234"))
                .authorities(CUSTOMER.getGrantedAuthorities())
                .build();

        return new InMemoryUserDetailsManager(
                elchin,
                ilqar,
                lale
        );
    }
}
